#Taxi Service Manager

Este o aplicatie web prin care se poate realiza o comunicare rapida intre client si companiile de taxi.  Informatiile 
cunoscute despre client vor fi numele si locatia. Acesta va putea fi capabil de a face cerere de taxi si de a anula cererea 
in cazul in care considera ca timpul de asteptare este prea mare. Dupa anulare, clientul va putea face cerere la alta companie,
care ii va trimite taxi intr-un timp mai scurt. Companiile de taxi vor dispune fiecare de un numar de taxiuri libere sau 
ocupate, iar fiecare taxi va avea o locatie si un timp de asteptare dupa acesta in functie de locatie.
    Aceasta aplicatie va realiza o comunicare mai rapida intre client si compania de taxi decat convorbirea telefonica. 
Va exista si un admin care va putea sa adauge companii de taxi.
    Utilizez design pattern-ul factory pentru a seta tarifele taxiurilor in functie de ora la care s-a facut comanda. De asemenea
folosesc design pattern-ul observer pentru a observa disponibilitatea masinilor de taxi.

    Diagrama pachete:

![N|Solid](https://i.ibb.co/gt30rSJ/diagrama-Pachet.png)

    Diagrama UML:
    
![N|Solid](https://i.ibb.co/jfvYqg3/classUML.png)

    Diagrama bazei de date:

![N|Solid](https://i.ibb.co/ykFDwFk/Taxi-Service-DBDiagram.png)
    
    
    
    
    
    
    
    