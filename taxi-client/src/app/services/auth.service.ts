import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/car.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedStatus = new BehaviorSubject<User>(null);
  constructor() { }
}
