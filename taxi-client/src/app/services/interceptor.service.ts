import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/car.model';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let user: User | string = localStorage.getItem('user');
        if (!user) {
            return next.handle(request);
        }

        user = JSON.parse(user) as User;

        request = request.clone({
            setHeaders: {
                Authorization: `Basic ${btoa(user.username+':'+user.password)}`
            }
        });
    return next.handle(request);
    }
}