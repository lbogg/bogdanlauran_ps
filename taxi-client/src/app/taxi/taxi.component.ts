import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Car, User} from '../models/car.model';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-taxi',
  templateUrl: './taxi.component.html',
  styleUrls: ['./taxi.component.css']
})
export class TaxiComponent implements OnInit {
  cars: Observable<Car[]>;
  user: User;

  constructor(private http: HttpClient, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.cars = this.http.get<Car[]>('/api/cars');
    this.auth.loggedStatus.subscribe(user => this.user = user);
  }

  gotoRequests(id: string) {
    if (this.user.role !== 'CLIENT') {
      return;
    }

    this.router.navigate(['/request/' + id])
  }

}
