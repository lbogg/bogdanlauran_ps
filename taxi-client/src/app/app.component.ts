import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private auth: AuthService) {}

  ngOnInit() {
    const user = localStorage.getItem('user');
    if (!user) {
      return;
    }

    this.auth.loggedStatus.next(JSON.parse(user));
  }
}
