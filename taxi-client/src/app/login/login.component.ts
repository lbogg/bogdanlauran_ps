import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/car.model';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private http: HttpClient, private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(event, username: HTMLInputElement, password: HTMLInputElement) {
    event.preventDefault();
    console.log(username.value, password.value);

    this.http.get<User>("/api/users/profile", {headers: { 'Authorization': `Basic ${btoa(username.value + ':' + password.value)}` } })
      .subscribe(user => {
        this.auth.loggedStatus.next(user);
        localStorage.setItem('user', JSON.stringify(user));
        this.router.navigate(['']);
        
      }, err => alert('cont nu k'));
  }

}
