import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {User} from "../models/car.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <a class="navbar-brand" href="#">Taxi RO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"  routerLinkActive="active">
              <a class="nav-link" [routerLink]="['/taxi']">Taxi</a>
            </li>
            <li class="nav-item" *ngIf="logged && user.role === 'CLIENT'" routerLinkActive="active">
              <a class="nav-link" [routerLink]="['/requests']">Cereri</a>
            </li>
            <li class="nav-item" *ngIf="logged && user.role === 'COMPANY'" routerLinkActive="active">
              <a class="nav-link" (click)="addCar()">Adauga Taxi</a>
            </li>
            <li class="nav-item" *ngIf="logged && user.role === 'ADMIN'" routerLinkActive="active">
              <a class="nav-link" routerLink="/register-company">Adauga Companie</a>
            </li>
          </ul>
          <ul class="navbar-nav nav-right">
            <li *ngIf="!logged" class="nav-item">
              <a class="nav-link" [routerLink]="['/login']">Login</a>
            </li>
            <li *ngIf="!logged" class="nav-item">
              <a class="nav-link" [routerLink]="['/register-user']">Inregistrare</a>
            </li>

            <li *ngIf="logged" class="nav-item">
              <a class="nav-link" [routerLink]="['/login']" (click)="logout()">Logout</a>
            </li>

            <li *ngIf="logged" class="nav-item">
              <strong class="nav-link">{{user.name}}</strong>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logged = false;
  user: User;

  constructor(public auth: AuthService, private http: HttpClient) { }

  ngOnInit() {
    this.auth.loggedStatus.subscribe(isLogged => {
      this.logged = !!isLogged;
      this.user = isLogged;
    });
  }

  logout() {
    this.auth.loggedStatus.next(null);
    localStorage.clear();
  }

  addCar() {
    if(!confirm("Sigur doresti sa adaugi o masina noua?")) {
      alert('NU s-a adaugat masina noua');
    }

    this.http.post<any>('/api/cars', {}).toPromise().then(() => alert("S-a adaugat o masina noua cu succes!"))
  }

}
