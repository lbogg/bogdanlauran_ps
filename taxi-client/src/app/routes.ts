import { Routes } from "@angular/router";
import { TaxiComponent } from "./taxi/taxi.component";
import { LoginComponent } from './login/login.component';
import {RequestComponent} from "./request/request.component";
import {RequestsComponent} from "./requests/requests.component";
import {RegisterCompanyComponent} from "./register-company/register-company.component";
import {RegisterUserComponent} from "./register-user/register-user.component";

export const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/taxi'},
    { path: 'taxi', component: TaxiComponent },
    { path: 'login', component: LoginComponent },
    { path: 'request/:id', component: RequestComponent },
    { path: 'requests', component: RequestsComponent },
    { path: 'create-car', component: RequestsComponent },
    { path: 'register-company', component: RegisterCompanyComponent },
    { path: 'register-user', component: RegisterUserComponent },
];
