export interface Car {
    available: boolean;
    client?: User;
    company?: User;
    id: number;
    tarif: number;
    waitTime: number;
}

export interface User {
    id: number;
    username: string;
    password?: string;
    name: string;
    role: string;
}
