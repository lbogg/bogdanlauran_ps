import {Car, User} from "./car.model";

export interface RequestDto {
  taxiCarId: number;
  time: Date;
  destination: string;
}

export interface Request {
  id: number,
  requestTime: Date,
  client: User,
  taxiCar: Car,
  destination1: string
}
