import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Input() isCompany: boolean;

  constructor(private http: HttpClient) { }

  ngOnInit() {

  }

  submit(username: string, password: string, name: string) {
    this.http.post('/api/users?company=' + this.isCompany, { username, password, name }).toPromise().then(() => alert('Utilizator creat cu succes')).catch(() => alert('Nu s-a putut crea utilizatorul'))
  }

}
