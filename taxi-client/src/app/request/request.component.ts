import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Car} from "../models/car.model";
import {Observable} from "rxjs/index";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  taxi: Observable<Car>

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {
    this.taxi = this.http.get<Car>('/api/cars/one?id=' + this.route.snapshot.params['id']);
  }

  makeRequest(destinatie: string) {
    this.http.post('/api/requests', {
      taxiCarId: +this.route.snapshot.params['id'],
      destination: destinatie
    }).toPromise().then(() => alert('S-a trimis cererea cu succes!')).catch(() => alert("Nu s-a trimis cererea cu succes!"))
  }

}
