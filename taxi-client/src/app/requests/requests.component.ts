import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/index";
import {Request} from "../models/request.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
  requests: Observable<Request[]>;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.requests = this.http.get<Request[]>("/api/requests/my-requests");
  }

}
