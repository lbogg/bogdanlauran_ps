package com.taxiService.taxi;

import com.taxiService.taxi.models.PriceFactory;
import com.taxiService.taxi.models.TaxiCar;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.RequestDao;
import com.taxiService.taxi.models.data.TaxiCarDao;
import com.taxiService.taxi.models.data.UserDao;
import com.taxiService.taxi.services.TaxiService;
import com.taxiService.taxi.utils.RegisterDto;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TaxiApplicationTests {

	@Test
	public void priceTest() {
		TaxiCar taxiCar = new TaxiCar();
		taxiCar.setRequestTime(LocalDateTime.now());
		taxiCar.setTarif();
		Assertions.assertEquals(PriceFactory.getPrice(LocalDateTime.now()).price(),taxiCar.getTarif());
	}

	@Test
	public void observerTest() {
		TaxiCar taxiCar = new TaxiCar();
		TaxiCar taxiCar1 = new TaxiCar();
		User user = new User();
		user.setTaxi(taxiCar1);
		taxiCar1.addObserver(user);
        taxiCar1.setAvailable(false);
        taxiCar.setAvailable(true);
		//Assertions.assertEquals();
	}

	@Test
    public void createUserTest(){
        TaxiCarDao taxiCarDao;
        RequestDao requestDao;
        RegisterDto registerDto = new RegisterDto();
        registerDto.setName("BigBoy");
        registerDto.setUsername("bigboy200");
        registerDto.setPassword("blabla");

        //TaxiService taxiService = new TaxiService();
    }
}
