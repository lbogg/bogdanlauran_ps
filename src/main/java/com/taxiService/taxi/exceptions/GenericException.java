package com.taxiService.taxi.exceptions;

public class GenericException extends RuntimeException {
    public GenericException(String msg) {super(msg);}
}
