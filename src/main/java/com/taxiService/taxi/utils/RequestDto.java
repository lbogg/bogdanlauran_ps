package com.taxiService.taxi.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class RequestDto {
    private long taxiCarId;
    private LocalDateTime time;
    private String destination;

    public RequestDto(){}

    public long getTaxiCarId() {
        return taxiCarId;
    }

    public void setTaxiCarId(long taxiCarId) {
        this.taxiCarId = taxiCarId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
