package com.taxiService.taxi.config;

import com.taxiService.taxi.models.Role;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final UserDao userDao;

    @Autowired
    public WebSecurity(UserDao userDao) {
        this.userDao = userDao;
    }

    public static class UserDetailsImpl implements UserDetails {
        final User user;

        public UserDetailsImpl(User user) {
            this.user = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().name()));
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.authorizeRequests()
//                .antMatchers("/login*", "/login/**", "/register").anonymous()
//          //      .antMatchers("/resources/**", "/").permitAll()
//           //     .antMatchers("/cars").permitAll()
//                .antMatchers("/requests").hasRole(Role.CLIENT.name())
//                .antMatchers(HttpMethod.POST,"/requests").hasRole(Role.CLIENT.name())
//                .antMatchers("/users/profile").authenticated()
          //      .antMatchers("/taxicar/{id}/delete").hasAuthority("ROLE_" + Role.COMPANY.name())
          //      .antMatchers("/manageCompanies").hasAuthority("ROLE_" + Role.ADMIN.name())
                .antMatchers("/**").permitAll()
                .anyRequest().permitAll()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().httpBasic()

                .and().csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authentication) throws Exception {
        authentication.userDetailsService(new UserServiceImpl());
       // authentication.userDetailsService(username ->new UserDetailsImpl(userDao.findByUsername(username).orElseThrow(EntityNotFoundException::new)));
    }

    class UserServiceImpl implements UserDetailsService {

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            User user = userDao.findByUsername(username).orElseThrow(EntityNotFoundException::new);

            return new UserDetailsImpl(user);
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword.toString().equals(encodedPassword);
            }
        };
    }
}
