package com.taxiService.taxi.services;

import com.taxiService.taxi.exceptions.GenericException;
import com.taxiService.taxi.models.Request;
import com.taxiService.taxi.models.Role;
import com.taxiService.taxi.models.TaxiCar;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.RequestDao;
import com.taxiService.taxi.models.data.TaxiCarDao;
import com.taxiService.taxi.models.data.UserDao;
import com.taxiService.taxi.utils.RegisterDto;
import com.taxiService.taxi.utils.RequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.logging.Logger;

@Service
public class TaxiService {

    private UserDao userDao;
    private RequestDao requestDao;
    private TaxiCarDao taxiCarDao;

    @Autowired
    public TaxiService(UserDao userDao,RequestDao requestDao,TaxiCarDao taxiCarDao){
        this.requestDao = requestDao;
        this.taxiCarDao = taxiCarDao;
        this.userDao = userDao;
    }

    /**
     * Metoda este utilizata pentru a gasi toate companiile de taxi din baza de date
     * @return o lista cu toti userii care au rolul de companie de taxi
     */
    public List<User> findCompanies(){
        return userDao.findAllByRole(Role.COMPANY);
    }

    /**
     * Metoda este folosita pentru procesul de inregistrare a aplicatiei
     * @param registerDto
     * @throws GenericException
     */
    public void createUser(RegisterDto registerDto) throws GenericException {
        User user = new User();
        if(registerDto.getName() == null || registerDto.getName().length() < 3 ||
                registerDto.getUsername() == null || registerDto.getUsername().length() < 3 ||
                registerDto.getPassword() == null || registerDto.getPassword().length() < 3) {
            throw new GenericException("All fields must have al least 3 characters.");
        }

        user.setRole(Role.CLIENT);
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setName(registerDto.getName());

        userDao.save(user);
    }

    public List<TaxiCar> getTaxis(){return taxiCarDao.findAll();}

    /**
     * Metoda folosita pentru stergerea unui user in functie de id.
     * @param id id-ul userului pe care dorim sa-l stergem
     */
    public void removeUser(long id){userDao.deleteById(id);}

    /**
     * Metoda este utilizata pentru a adauga un user companie de taxi.
     */
    public void addUser(){
        User user = new User();

        user.setTaxi(null);
        user.setTaxiCompanyAvailableCars(null);
        user.setRole(Role.COMPANY);
    }

    public void removeTaxiCar(long id) {taxiCarDao.deleteById(id);}

    /**
     * Metoda prin care o companie de taxi isi adauga noi masini in sistem.
     */
    public void addTaxiCar() {
        TaxiCar taxiCar = new TaxiCar();

        User user = userDao.findByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).orElseThrow(EntityNotFoundException::new);

        taxiCar.setAvailable(true);
        taxiCar.setRequestTime(null);
        taxiCar.setClient(null);
        taxiCar.setTarif();
        taxiCar.setCompany(user);

        taxiCarDao.save(taxiCar);
    }

    /**
     * Metoda prin care clientul poate sa comande taxi. Request-ul este facut doar daca taxiul nu este ocupat,
     * altfel clientul trebuie sa astepte pana cand acesta devine valabil.
     * @param requestDto
     * @throws GenericException
     */
    public Request makeRequest(RequestDto requestDto) {

        LocalDateTime time = LocalDateTime.now();

        TaxiCar taxiCar = taxiCarDao.findById(requestDto.getTaxiCarId()).orElseThrow(EntityNotFoundException::new);

        if(!taxiCar.isAvailable()){
            throw new GenericException("Taxi not available for the moment."); // pai e ok asa
        }

        taxiCar.setRequestTime(time);
        taxiCar.setTarif();
        taxiCar.setAvailable(false);//io aicea il fac false..unde il fac true dupa ce o trecut waitTimehmmmmm
        taxiCar.setWaitTime((int)(Math.random() * 10 + 10));

        Request request = new Request();
        request.setRequestTime(LocalDateTime.now());
        request.setClient(userDao.findByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).orElseThrow(EntityNotFoundException::new));
        request.setDestination1(requestDto.getDestination());
        request.setComplete(false);
        request.setTaxiCar(taxiCar);
        requestDao.save(request);

        Runnable th = () -> {
            try {
                TaxiCar car = taxiCarDao.findById(requestDto.getTaxiCarId()).orElseThrow(EntityNotFoundException::new);
                Thread.sleep(car.getWaitTime() * 1000);
                car.setAvailable(true);
                taxiCarDao.save(car);
                System.out.println("taxi " + car.getId() + " e available");

                request.setComplete(true);
                requestDao.save(request);

            } catch (InterruptedException e) {
                System.out.println("nu mers waititme: " + e.getMessage());
            }
        };
        new Thread(th).start();



        return request;
    }

}
