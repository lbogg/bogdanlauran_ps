package com.taxiService.taxi.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    private static Connection connection;

    public DbConnection() {
    }

    public static Connection getConnection() {
        try {
            if (connection == null) {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/taxi_app");
                System.out.println("connected");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
