package com.taxiService.taxi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Observable;

@Entity
public class TaxiCar extends Observable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private LocalDateTime requestTime;
    private int waitTime;
    private boolean available;
    private float tarif;
    @OneToOne
    private Request request;
    @ManyToOne
    private User company;
    @OneToOne
    private User client;

    public TaxiCar(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(LocalDateTime requestTime) {
        this.requestTime = requestTime;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
        setChanged();
        notifyObservers(available);
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public User getCompany() {
        return company;
    }

    public void setCompany(User company) {
        this.company = company;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public float getTarif() {
        return tarif;
    }

    public void setTarif() {
        LocalDateTime time = requestTime;
        if (time == null) {
            time = LocalDateTime.now();
        }
       this.tarif = PriceFactory.getPrice(time).price();
    }

    @Override
    public String toString() {
        return "TaxiCar{" +
                "id=" + id +
                ", requestTime=" + requestTime +
                ", waitTime=" + waitTime +
                ", available=" + available +
                ", tarif=" + tarif +
                ", request=" + request +
                ", client=" + client +
                '}';
    }
}
