package com.taxiService.taxi.models;


import java.time.LocalDateTime;
import java.time.LocalTime;

public class PriceFactory {

    /**
     * Metoda de factory prin care setez tariful unui taxi in functie de timpul la care acesta
     * este chemat pentru o comanda.
     * @param timeNow timpul la care este facuta comanda.
     * @return 3 tipuri de tarife, tariful de zi(1.7), tariful de noapte(2.5) si tariful in perioada de festival(3.5).
     */
    public static TaxiPrice getPrice(LocalDateTime timeNow){
        if(timeNow.getHour() < 20 && timeNow.getHour() > 9){
            return new TaxiPriceByDay();
        }
        else if(timeNow.getHour() < 9){
            return  new TaxiPriceByNight();
        }
        else return new FestivalTaxiPrice();
    }
}
