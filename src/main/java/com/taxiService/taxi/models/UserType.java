package com.taxiService.taxi.models;

public enum UserType {
    CLIENT, TAXICOMPANY;
}
