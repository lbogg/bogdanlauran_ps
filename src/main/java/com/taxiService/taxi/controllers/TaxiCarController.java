package com.taxiService.taxi.controllers;

import com.taxiService.taxi.models.TaxiCar;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.TaxiCarDao;
import com.taxiService.taxi.services.TaxiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin("*")
@RestController
@RequestMapping("/cars")
public class TaxiCarController {

    @Autowired
    TaxiCarDao taxiCarDao;

    @Autowired
    TaxiService taxiService;

    @GetMapping
    public List<TaxiCar> index(){
        return taxiCarDao.findAll().parallelStream().peek(car -> {
            if (car.getRequest() != null) {
                car.getRequest().setTaxiCar(null);
            }

            if(car.getClient() != null) {
                car.getClient().setTaxiCompanyAvailableCars(null);
                car.getClient().setTaxiCompanyCars(null);
            }

            car.getCompany().setTaxi(null);
        }).collect(Collectors.toList());
    }

    @PostMapping
    public void create(){
        taxiService.addTaxiCar();
    }

    @GetMapping("/one")
    public TaxiCar findOne(@Param("id") long id){
        TaxiCar car = taxiCarDao.findById(id).orElseThrow(EntityNotFoundException::new);
        if (car.getRequest() != null) {
            car.getRequest().setTaxiCar(null);
        }

        if(car.getClient() != null) {
            car.getClient().setTaxiCompanyAvailableCars(null);
            car.getClient().setTaxiCompanyCars(null);
        }
        car.getCompany().setTaxi(null);

        return car;
    }

}
