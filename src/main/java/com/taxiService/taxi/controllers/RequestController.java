package com.taxiService.taxi.controllers;

import com.taxiService.taxi.models.Request;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.RequestDao;
import com.taxiService.taxi.services.TaxiService;
import com.taxiService.taxi.utils.RequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.xml.ws.Action;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("requests")
public class RequestController {

    @Autowired
    RequestDao requestDao;

    @Autowired
    private TaxiService taxiService;

    @GetMapping
    public List<Request> displayRequests(){
        return requestDao.findAll().parallelStream().peek(request -> {
            request.getClient().setClientRequests(null);
            request.getTaxiCar().setRequest(null);
        }).collect(Collectors.toList());
    }

    @GetMapping("/my-requests")
    public List<Request> displayMYRequests(){
        String username = ((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return requestDao.findAll().parallelStream().peek(request -> {
            request.getClient().setClientRequests(null);
            request.getTaxiCar().setRequest(null);
        }).filter(req -> req.getClient().getUsername().equals(username)).collect(Collectors.toList());
    }

    @PostMapping
    public Request create(@RequestBody RequestDto requestDto) {
        Request request = taxiService.makeRequest(requestDto);
        request.getTaxiCar().setRequest(null);
        request.getClient().setClientRequests(null);

        return request;
    }
}
