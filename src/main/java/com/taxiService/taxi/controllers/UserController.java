package com.taxiService.taxi.controllers;

import com.taxiService.taxi.models.Role;
import com.taxiService.taxi.models.User;
import com.taxiService.taxi.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    UserDao userDao;

    @GetMapping
    public List<User> displayUsers(){
        return userDao.findAll();
    }

    @GetMapping("/profile")
    public User profile() {
        return userDao.findByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping
    public void create(@RequestBody User user, @Param("company") boolean company) {
        User u = new User();
        u.setRole(company ? Role.COMPANY : Role.CLIENT);
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        u.setName(user.getName());
        userDao.save(u);
    }
}
